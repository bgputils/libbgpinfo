#ifndef BGPDUMPPARSER_H
#define BGPDUMPPARSER_H

#include <string> 
#include <thread>
#include <set>
#include <vector>
#include <Poco/Exception.h>
#include <Poco/Runnable.h>
extern "C" {
#include <bgpdump_lib.h>
}
#include <dump.h>
#include <prefix.h>
#include <asrelation.h>
#include <bgpinfo.pb.h>


namespace BGPInfo {

    /**
     * Parses BGPDump MRT files into libbgpinfo file structure
     * This class takes a MRT file as source and writes it to the defined out path.
     */
    class DumpParser : public Poco::Runnable {
    public:
        virtual void run();
        DumpParser();

        /**
         * @param pathToDumpFile Path to the BGP dump file to convert
         * @param destinationDirectory Target directory
         */
        DumpParser(std::string pathToDumpFile, std::string destinationDirectory);

    private:
        std::map<uint32_t, BGPInfo::ASRelation> _asRelations;
        /**
         * Unordered map for collecting prefixes. Writing/lookup performance of
         * unordered_set is O(1) which is much faster than the (O log n) of the
         * set used in Prefixes. 
         */
        std::unordered_set<BGPInfo::Prefix>_prefixes; 
        std::string _destinationDirectory; //target directory
        std::string _pathToDumpFile; //path to the dump file to read

        /**
         * always wait for n "networks for route grouping" to stack up until 
         * routes get written this slightly increases the performance
         */
        size_t _bulkwritesize = 5;

        /**
         * helper to detect if "network" for route grouping has changed
         */
        Poco::Net::IPAddress _lastNetwork; 

        /**
         * Processes the dump file
         * @return 
         */
        BGPInfo::Dump processDumpFile();

        /**
         * Processes a dump file entry
         * @param entry A BGP dump entry
         * @param dump The BGP Info dump
         */
        void process(BGPDUMP_ENTRY *entry, BGPInfo::Dump* dump);


        /**
         * Writes the routes in the buffer to file.
         * The parser is not holding all routes in memory as this would use too much memory.
         */
        void writeRoutes();

    };
}
#endif /* BGPDUMPPARSER_H */

