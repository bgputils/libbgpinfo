#include <cstdlib>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>
#include <Poco/Util/Application.h>
#include <sys/stat.h>
#include <Poco/ThreadPool.h>
#include <Poco/Mutex.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>    
#include <fstream>

#include <util.h>
#include "dumpparser.h"


using namespace std;

/**
 * Displays the help dialog
 * @param name The program name
 */
static void showUsage(std::string name) {
    std::cerr << "Usage: " << name << " -f/-s -d\n"
            << "Options:\n"
            << "\t-h\t\tShow this help message\n"
            << "\t-f FILE\tSpecify the source file containing a list of MRT BGP dump files\n"
            << "\t-s SOURCE\tSpecify a comma separated list of MRT BGP dump files\n"
            << "\t-d DESTINATION\tSpecify the destination folder. Will be created if it is not existing\n"
            << std::endl;
}

int main(int argc, char** argv) {

    std::string sourcefile;
    std::vector<std::string> files;
    std::string destinationDirectory;
    std::string fileList;

    //Input handling
    if (argc < 3) {
        showUsage(argv[0]);
        return 1;
    }
    int c;
    while ((c = getopt(argc, argv, "hf:s:d:")) != -1) {
        switch (c) {
            case 'h':
                showUsage(argv[0]);
                break;
            case 'f':
                sourcefile = optarg;
                break;
            case 's':
                fileList = optarg;
                break;
            case 'd':
                destinationDirectory = optarg;
                break;
            case '?':
                showUsage(argv[0]);
                return 1;
            default:
                abort();
        }
    }

    if (fileList.empty() && sourcefile.empty()) {
        std::cerr << "One source option (-f,-s) must be set." << std::endl;
        showUsage(argv[0]);
    }

    if (!fileList.empty() && !sourcefile.empty()) {
        std::cerr << "Only one source option (-f,-s) must be set." << std::endl;
        showUsage(argv[0]);
    }

    if (destinationDirectory.empty()) {
        std::cerr << "The destination directory must be specified" << std::endl;
        showUsage(argv[0]);
    }

    if (!sourcefile.empty()) {

        if (BGPInfo::Util::fileExists(sourcefile.c_str()) == false) {
            std::cerr << "The source file does not exist." << std::endl;
            showUsage(argv[0]);
        }
        std::cout << sourcefile << std::endl;
        ifstream infile(sourcefile);
        std::string file;
        while (infile >> file) {
            files.push_back(file);
        }
    }

    if (!fileList.empty()) {
        files = BGPInfo::Util::split(fileList, ',');
    }


    BGPInfo::DumpParser runnables[files.size()];
    int i = 0;
    for (auto const& file : files) {
        runnables[i] = BGPInfo::DumpParser(file, destinationDirectory);
        cout << "start processing: " << file << endl;
        Poco::ThreadPool::defaultPool().start(runnables[i]);
        i++;
    }
    Poco::ThreadPool::defaultPool().joinAll();
    cout << "processing BGP dumps done" << endl;
    return 0;
}