#include <cstdlib>
extern "C" {
#include "bgpdump_lib.h"
}

#include <unordered_map>

#include <time.h>

#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <algorithm>

#include <iostream>     
#include <errno.h>
#include <stdio.h>
#include <ctime>

#include <sstream>
#include <vector>
#include <bitset>
#include <cstdio>
#include <iomanip>
#include <libgen.h>
#include <mutex>

#include <fstream>

#include <Poco/Path.h>
#include <Poco/Net/NetException.h>

#include <util.h>
#include <route.h>


#include "dumpparser.h"
#include "config.h"


using namespace std;

BGPInfo::DumpParser::DumpParser() {

}

BGPInfo::DumpParser::DumpParser(std::string pathToDumpFile, std::string destinationDirectory) {
    this->_pathToDumpFile = pathToDumpFile;
    this->_destinationDirectory = destinationDirectory;
}

void BGPInfo::DumpParser::run() {
    this->processDumpFile();
}

BGPInfo::Dump BGPInfo::DumpParser::processDumpFile() {
    BGPDUMP *my_dump;
    BGPDUMP_ENTRY *my_entry = NULL;

    Poco::Path p(_pathToDumpFile);
    BGPInfo::Dump* dump = new BGPInfo::Dump();
    my_dump = bgpdump_open_dump(_pathToDumpFile.c_str());
    if (my_dump == NULL) {
        throw std::runtime_error("Error opening dump file");
    }

    int count = 0;
    do {
        my_entry = bgpdump_read_next(my_dump);

        if (my_entry != NULL) {
            if (count == 0) {
                dump = new BGPInfo::Dump(p.getBaseName(), my_entry->time);
                dump->setPath(_destinationDirectory);
                count++;
            }

            process(my_entry, dump);
            bgpdump_free_mem(my_entry);

        }
    } while (my_dump->eof == 0);
    //write remaining routes;
    dump->saveRoutes(true);

    BGPDUMP_TABLE_DUMP_V2_PEER_INDEX_TABLE * pt = my_dump->table_dump_v2_peer_index_table;
    for (int i = 0; i < pt->peer_count; i++) {
        dump->addPeer(Peer(Util::parseIP(pt->entries[i].peer_ip, pt->entries[i].afi)
                , Util::parseIP(pt->entries[i].peer_bgp_id), pt->entries[i].peer_as));
    }

    dump->savePeers();

    dump->setPrefixes(std::set<BGPInfo::Prefix>(_prefixes.begin(), _prefixes.end()));
    dump->savePrefixes();
    dump->setASRelations(_asRelations);
    dump->saveASRelations();
    dump->save();

    return *dump;
}

void BGPInfo::DumpParser::process(BGPDUMP_ENTRY *entry, BGPInfo::Dump *dump) {
    BGPDUMP_TABLE_DUMP_V2_PREFIX *e;
    if (entry->type != BGPDUMP_TYPE_TABLE_DUMP_V2) return;

    e = &entry->body.mrtd_table_dump_v2_prefix;

    /*
     * Defines the CIDR netmask that is used to split up the routes
     * for example would 32.25.5.4 be part of 32.0.0.0 routes (with netmask 8).
     */
    int mask = BGPInfo::Config::kRoutesSplitMaskIPv4;
    if (e->afi == AFI_IP6) {
        mask = BGPInfo::Config::kRoutesSplitMaskIPv6;
    }
    Poco::Net::IPAddress currentNetwork =
            Util::maskIP(Util::parseIP(e->prefix, e->afi), mask);

    if (currentNetwork != _lastNetwork) {
        if (dump->getRouteCount() >= _bulkwritesize) {
            dump->saveRoutes(true);
        }
        _lastNetwork = currentNetwork;
    }

    //some error checking
    if (e->afi != AFI_IP && e->afi != AFI_IP6) {
        throw std::runtime_error("Error: BGP table dump version 2 entry "
                "with unknown subtype");
    }
    BGPInfo::Prefix pf;

    //for each route entry
    for (int i = 0; i < e->entry_count; i++) {

        attributes_t *attr = e->entries[i].attr;

        if (attr != NULL) {
            if ((attr->flag & ATTR_FLAG_BIT(BGP_ATTR_AS_PATH)) != 0) {
                std::string asn = "-1";
                uint32_t asnI = -1;
                std::string strpath = attr->aspath->str;
                std::string setStartChar = " {";
                std::string token = strpath.substr(0, strpath.find(setStartChar));

                //split route path to get target AS number
                //we do want continue reading even if there is a parsing error
                try {
                    std::vector<std::string> hops = Util::split2(token, ' ');
                    if (hops.empty() == false) {
                        for (uint32_t j = 0; j < hops.size(); j++) {
                            //ignore duplicates
                            if (asn.compare(hops[j]) == 0) {
                                continue;
                            }
                            asn = hops[j];
                            try {
                                asnI = stoul(asn);
                            } catch (const std::invalid_argument& ia) {
                                std::cerr << "Invalid argument: " << ia.what()
                                        << " asn: " << asn << "length: "
                                        << asn.length() << std::endl;
                            } catch (const std::out_of_range & oor) {
                                std::cerr << "Out of range: " << oor.what()
                                        << " asn: " << asn << "length: "
                                        << asn.length() << std::endl;
                            }

                            auto asit = _asRelations.find(asnI);
                            BGPInfo::ASRelation asrel(asnI);
                            if (asit != _asRelations.end()) {
                                asrel = asit->second;
                            }

                            if (j < hops.size() - 1) {
                                asrel.addRight(stol(hops[j + 1]));
                            }
                            if (j > 0) {
                                asrel.addLeft(stol(hops[j - 1]));
                            }
                            _asRelations[asnI] = asrel;
                        }
                    }
                } catch (const std::invalid_argument& ia) {
                    std::cerr << "Invalid argument: " << ia.what() << " path: "
                            << attr->aspath->str << " asn: " << asn << std::endl;
                }

                //generate the prefix
                if (e->afi == AFI_IP) {
                    pf = BGPInfo::Prefix(&e->prefix.v4_addr, e->prefix_length);
                } else {
                    pf = BGPInfo::Prefix(&e->prefix.v6_addr, e->prefix_length);
                }
                //retrieve prefix if it already exists
                std::unordered_set<BGPInfo::Prefix>::iterator pit = _prefixes.find(pf);
                //remove the prefix from list as in place updates are not possible
                if (pit != _prefixes.end()) {
                    pf = *pit;
                    _prefixes.erase(pf);
                }

                //Add ASN to the list
                pf.addASN(asnI);
                _prefixes.insert(pf); //push prefix on the list

                //collect route values
                std::vector<BGPInfo::Route> routes;
                std::string community, ecommunity, lcommunity;
                if (attr->community != NULL) {
                    community = std::string(attr->community->str);
                }
                //                ecommunity struct not defined in libbgpdump...
                //                if (attr->ecommunity != NULL) {
                //                    ecommunity = std::string(aattr->ecommunity->str);
                //                }
                if (attr->lcommunity != NULL) {
                    lcommunity = std::string(attr->lcommunity->str);
                }
                int origin = 0;
                if ((attr->flag & ATTR_FLAG_BIT(BGP_ATTR_ORIGIN)) != 0) {
                    origin = attr->origin;
                }

                u_int32_t med = 0;
                if ((attr->flag & ATTR_FLAG_BIT(BGP_ATTR_MULTI_EXIT_DISC)) != 0) {
                    med = attr->med;
                }

                u_int32_t localpref = 0;
                if ((attr->flag & ATTR_FLAG_BIT(BGP_ATTR_LOCAL_PREF)) != 0) {
                    localpref = attr->local_pref;
                }


                BGPInfo::Route route(Util::parseIP(e->entries[i].peer->peer_bgp_id)
                        , origin
                        , e->entries[i].originated_time
                        , attr->aspath->str
                        , Util::parseIP(attr->nexthop)
                        , med
                        , localpref
                        , (attr->flag & ATTR_FLAG_BIT(BGP_ATTR_ATOMIC_AGGREGATE))
                        , community
                        , ecommunity
                        , lcommunity
                        , pf);
                dump->addRoute(route);
            }
        }
    }
}




