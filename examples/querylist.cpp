/**
 * Example usage for libbgpinfo
 * This application takes a list of IP addresses or AS numbers and returns all 
 * found route entries in the provided libbgpinfo data sources as JSON string.
 */

#include <cstdlib>
#include "Poco/ThreadPool.h"
#include "Poco/Runnable.h"
#include "Poco/Mutex.h"
#include <Poco/DynamicStruct.h>
#include <Poco/Path.h>
#include <iostream>
#include <time.h>
#include <algorithm>

#include <iostream>    
#include <fstream>
#include <stdio.h>

#include <vector>
#include <map>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>


#include <Poco/Net/IPAddress.h>


#include <dump.h>
#include <util.h>
#include <config.h>
#include <route.h>
#include <prefix.h>

using namespace std;

/**
 * Hold search results and defines the JSON output format
 */
struct SearchResult {
    typedef std::map<BGPInfo::Prefix, std::vector<BGPInfo::Route>> routeMap; //routes per prefix
    std::map<std::string, std::vector<routeMap>> routes; //routes per dump
    std::string searchType;
    std::string searchTerm;

    Poco::DynamicStruct toDynamicStruct() {
        Poco::DynamicStruct data;
        Poco::Dynamic::Array jdumps;
        data["SearchType"] = searchType;
        data["SearchTerm"] = searchTerm;
        for (auto &dump : routes) {
            Poco::DynamicStruct jdump;

            jdump["Name"] = dump.first;
            Poco::Dynamic::Array jprefixes;
            for (auto &prefixes : dump.second) {
                for (auto &prefix : prefixes) {
                    Poco::DynamicStruct jprefix;
                    std::ostringstream oss;
                    oss << prefix.first.getPrefix().toString() << "/" << prefix.first.getLength();
                    jprefix["Prefix"] = oss.str();
                    Poco::Dynamic::Array jasns;
                    for (int as : prefix.first.getASList()) {
                        jasns.push_back(as);
                    }
                    jprefix["AS"] = jasns;

                    Poco::Dynamic::Array jroutes;

                    for (auto &croute : prefix.second) {
                        Poco::DynamicStruct jroute;
                        jroute["Peer-ID"] = croute.getPeerId().toString();
                        time_t timeorigin = croute.getTimeOriginated();
                        jroute["Timestamp_orig"] = std::string(ctime(&timeorigin));
                        jroute["Path"] = croute.getPath();
                        jroute["Community"] = croute.getCommunity();

                        jroute["ECommunity"] = croute.getLcommunity();
                        jroute["LCommunity"] = croute.getEcommunity();
                        jroutes.push_back(jroute);
                    }
                    jprefix["Routes"] = jroutes;

                    jprefixes.push_back(jprefix);

                }
            }
            jdump["Prefixes"] = jprefixes;
            jdumps.push_back(jdump);
        }
        data["Dumps"] = jdumps;
        return data;
    }

    std::string toString() {
        return toDynamicStruct().toString();
    }
};

/**
 * Collections of search results and defines the JSON output format
 */
struct SearchResults {
    std::vector<SearchResult> results;

    std::string toString() {
        Poco::DynamicStruct data;
        Poco::Dynamic::Array jresults;
        for (SearchResult sr : results) {
            jresults.push_back(sr.toDynamicStruct());
        }
        data["Results"] = jresults;
        return data.toString();
    }
};

/**
 * Class to read bgpinfo dumps&prefixes threaded
 */
class ReadDumpRunnable : public Poco::Runnable {
public:

    ReadDumpRunnable() {

    }

    ReadDumpRunnable(Poco::FastMutex* lock, std::vector<BGPInfo::Dump>* dumps, std::string collectorpath) {
        _lock = lock;
        _dumps = dumps;
        _dumppath = collectorpath;

    }

    virtual void run() {
        BGPInfo::Dump dump = BGPInfo::Dump::load(_dumppath, false);
        dump.loadPrefixes();

        _lock->lock();
        _dumps->push_back(dump);
        _lock->unlock();
    }

private:
    std::vector<BGPInfo::Dump>* _dumps;
    Poco::FastMutex* _lock;
    std::string _dumppath;
};

/**
 * Retrieves the routes for the given prefixes from the dump
 * @param prefixes A list of prefixes,
 * @param dump A dump object responsible for the routes
 * @return A map of prefixes and their routes
 */
SearchResult::routeMap getRoutes(std::vector<BGPInfo::Prefix> &prefixes, BGPInfo::Dump &dump) {
    SearchResult::routeMap routelist;

    /*
     * For each found prefix get routes
     */
    for (BGPInfo::Prefix &prefix : prefixes) {
        dump.loadRoutes(prefix);


        int mask = BGPInfo::Config::kRoutesSplitMaskIPv4;
        Poco::Net::IPAddress net = prefix.getPrefix();
        if (net.family() == Poco::Net::IPAddress::Family::IPv6) {
            mask = BGPInfo::Config::kRoutesSplitMaskIPv6;
        }
        Poco::Net::IPAddress masked = BGPInfo::Util::maskIP(net, mask);

        auto routesm = dump.getRoutes();
        auto routes = routesm[masked];
        for (BGPInfo::Route route : routes) {
            if (route.getPrefix() == prefix) {
                routelist[prefix].push_back(route);
            }
        }


    }
    return routelist;
}

/**
 * Queries the provided dumps for the prefixes of the given IP address
 * @param ip The IP address to look for in the prefixes
 * @param dumps A list of dumps
 * @return A SearchResult object containing the found prefixes
 */
SearchResult findPrefixesByIP(Poco::Net::IPAddress ip, std::vector<BGPInfo::Dump> &dumps) {
    SearchResult result;
    result.searchTerm = ip.toString();
    result.searchType = "IP";

    Poco::Net::IPAddress lowIP, upIP;
    BGPInfo::Prefix lowPrefix, upPrefix;

    //check if IP is an IPv6 mapped IPv4 or a deprecated IPv4 compatible address
    if (ip.family() == Poco::Net::IPAddress::IPv6 && (ip.isIPv4Mapped() || ip.isIPv4Compatible())) {
        std::vector<std::string> splitip = BGPInfo::Util::split2(ip.toString(), ':');
        std::string sipv4 = splitip.back();
        ip = Poco::Net::IPAddress(sipv4);
    }
    Poco::Net::IPAddress::Family fam = ip.family();
    lowIP = Poco::Net::IPAddress(ip);
    upIP = Poco::Net::IPAddress(ip);


    //set lower and upper bound search filter IP type specific
    if (fam == Poco::Net::IPAddress::IPv4) {
        unsigned lowCDIR = 8;
        unsigned upCDIR = 28;
        Poco::Net::IPAddress lowermask = Poco::Net::IPAddress(lowCDIR, Poco::Net::IPAddress::IPv4);
        lowIP.mask(lowermask);
        lowPrefix = BGPInfo::Prefix(lowIP, lowCDIR);
        upPrefix = BGPInfo::Prefix(ip, upCDIR);
    } else {
        unsigned lowCDIR = 8;
        unsigned upCDIR = 64;
        Poco::Net::IPAddress lowermask = Poco::Net::IPAddress(lowCDIR, Poco::Net::IPAddress::IPv6);
        lowIP = lowIP & lowermask;
        lowPrefix = BGPInfo::Prefix(lowIP, lowCDIR);
        upPrefix = BGPInfo::Prefix(ip, upCDIR);
    }

    //search in each dump
    for (auto &dump : dumps) {

        std::vector<BGPInfo::Prefix> fPrefixes;
        std::set<BGPInfo::Prefix> sprefixes = dump.getPrefixes();

        //boundaries for filter lvl 1: Speeds up prefix querying
        auto low = sprefixes.lower_bound(lowPrefix);
        auto upper = sprefixes.upper_bound(upPrefix);

        for (auto it = low; it != upper; ++it) {
            BGPInfo::Prefix prefix = *it;

            if (prefix.getPrefix().family() != fam) {
                continue;
            }
            Poco::Net::IPAddress ipB = ip;
            auto mask = Poco::Net::IPAddress(prefix.getLength(), fam);

            //filter lvl 2
            if (fam == Poco::Net::IPAddress::IPv4) {
                ipB.mask(mask);
            } else {
                ipB = ipB & mask;
            }
            BGPInfo::Prefix prefixB(ipB, prefix.getLength());

            if (prefixB == prefix) {
                fPrefixes.push_back(prefix);
            }

        }

        result.routes[dump.getName()].push_back(getRoutes(fPrefixes, dump));
    }


    return result;
}

/**
 * Queries the provided dumps for the given AS number
 * @param asn The Autonomous System Number to look for
 * @param dumps A list of dumps
 * @return A SearchResult object containing the found prefixes
 */
SearchResult findPrefixesByASN(unsigned asn, std::vector<BGPInfo::Dump> dumps) {
    SearchResult result;
    result.searchTerm = std::to_string(asn);
    result.searchType = "ASN";

    for (BGPInfo::Dump dump : dumps) {
        std::vector<BGPInfo::Prefix> fPrefixes;
        for (auto const& prefix : dump.getPrefixes()) {
            auto const& asList = prefix.getASList();
            if (std::find(asList.begin(), asList.end(), asn) != asList.end()) {
                fPrefixes.push_back(prefix);
            }
        }
        result.routes[dump.getName()].push_back(getRoutes(fPrefixes, dump));
    }

    return result;
}

/**
 * Displays the help dialog
 * @param name The program name
 */
static void showUsage(std::string name) {
    std::cerr << "Usage: " << name << " -f [-i,-a]\n"
            << "Options:\n"
            << "\t-h\t\tShow this help message\n"
            << "\t-f FILE\tSpecify source file containing a list of the libbgpinfo data sources \n"
            << "\t-i IPLIST\tSpecify a file containing IP addresses to look for\n"
            << "\t-a ASNLIST\tSpecify a file containing AS numbers to look for\n"
            << std::endl;
}

int main(int argc, char** argv) {
    std::string sourcefile;
    std::string iplist;
    std::string aslist;
    int function = 0; // 0 = IP Search; 1 = ASN Search

    //Input handling
    if (argc < 3) {
        showUsage(argv[0]);
        return 1;
    }

    int c;
    while ((c = getopt(argc, argv, "hf:i:a:")) != -1) {
        switch (c) {
            case 'h':
                showUsage(argv[0]);
                break;
            case 'f':
                sourcefile = optarg;
                break;
            case 'i':
                iplist = optarg;
                break;
            case 'a':
                function = 1;
                aslist = optarg;
                break;
            case '?':
                showUsage(argv[0]);
                return 1;
            default:
                abort();
        }
    }

    if (sourcefile.empty() && iplist.empty() && aslist.empty()) {
        showUsage(argv[0]);
    }

    if (BGPInfo::Util::fileExists(sourcefile.c_str()) == false) {
        std::cerr << "The source file does not exist." << std::endl;
        showUsage(argv[0]);
    }

    if (function == 0 && BGPInfo::Util::fileExists(iplist.c_str()) == false) {
        std::cerr << "The IP list file does not exist." << std::endl;
        showUsage(argv[0]);
    }

    if (function == 1 && BGPInfo::Util::fileExists(aslist.c_str()) == false) {
        std::cerr << "The AS list file does not exist." << std::endl;
        showUsage(argv[0]);
    }

    std::vector<BGPInfo::Dump> dumps;
    
    //Read data sources
    try {
        
        ifstream dumpinfile(sourcefile);
        std::string collector;
        vector<std::string> sourcedumps;
        while (dumpinfile >> collector) {
            sourcedumps.push_back(collector);
        }

        Poco::FastMutex lock;
        ReadDumpRunnable runnables[sourcedumps.size()];
        int i = 0;
        //read prefixes
        for (std::string collectorpath : sourcedumps) {
            runnables[i] = ReadDumpRunnable(&lock, &dumps, collectorpath);
            Poco::ThreadPool::defaultPool().start(runnables[i]);
            i++;
        }
        //wait for dump importer to finish
        Poco::ThreadPool::defaultPool().joinAll();
    } catch (std::ifstream::failure e) {
        std::cerr << "Exception opening/reading/closing file " + sourcefile + "\n";
        exit(1);
    }

    SearchResults results;

    if (function == 0) {
        try {
            //read IP list
            ifstream ipinfile(iplist);
            string sip;
            vector<string> sips;
            while (ipinfile >> sip) {
                sips.push_back(sip);
            }

            for (string sip : sips) {
                Poco::Net::IPAddress ip(sip);
                results.results.push_back(findPrefixesByIP(ip, dumps));
            }
        } catch (std::ifstream::failure e) {
            std::cerr << "Exception opening/reading/closing file " + iplist + "\n";
            exit(1);
        }
    } else {
        try {
            //read AS list
            ifstream asninfile(aslist);
            std::string asn;
            vector<unsigned> asns;

            while (asninfile >> asn) {
                asns.push_back(std::stoul(asn));
            }

            for (unsigned asn : asns) {
                results.results.push_back(findPrefixesByASN(asn, dumps));
            }
        } catch (std::ifstream::failure e) {
            std::cerr << "Exception opening/reading/closing file " + aslist + "\n";
            exit(1);
        } catch (std::invalid_argument e) {
            std::cerr << "Non numerical value found in ASN list\n";
            exit(1);
        }
    }
    cout << results.toString() << endl;

    return 0;
}


