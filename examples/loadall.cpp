/**
 * Example usage for libbgpinfo
 * This program demonstrates the the memory usage when a whole dump is 
 * loaded at once.
 */


#include <cstdlib>

#include <dump.h>
#include <util.h>
#include <exception>

using namespace std;

/**
 * Displays the help dialog
 * @param name The program name
 */
static void showUsage(std::string name) {
    std::cerr << "Usage: " << name << " -d\n"
            << "Options:\n"
            << "\t-h\t\tShow this help message\n"
            << "\t-d DUMP\tSpecify a dump to load\n"
            << std::endl;
}

int main(int argc, char** argv) {
    std::string dumpfile;

    //Input handling
    if (argc < 3) {
        showUsage(argv[0]);
        return 1;
    }

    int c;
    while ((c = getopt(argc, argv, "hd:")) != -1) {
        switch (c) {
            case 'h':
                showUsage(argv[0]);
                break;
            case 'd':
                dumpfile = optarg;
                break;
            case '?':
                showUsage(argv[0]);
                return 1;
            default:
                abort();
        }
    }

    
    std::cout << "this may take some time ..."<< endl;
    if (BGPInfo::Util::fileExists(dumpfile.c_str()) == false) {
        std::cout << "The source file does not exist." << std::endl;
        showUsage(argv[0]);
        return 1;
    }


    //Read dump
    try {
        BGPInfo::Dump dump = BGPInfo::Dump::load(dumpfile, true);
        std::cout << "loaded: " << dump.getName() << std::endl;
        std::cout << "\t prefixes: " << dump.getPrefixCount() << std::endl;
        std::cout << "\t route lists: " << dump.getRouteCount() << std::endl;
        std::cout << "\t peers: " << dump.getPeerCount() << std::endl;
        std::cout << "\t AS: " << dump.getASCount()<< std::endl;
    } catch (char const *e) {
        std::cout << e << std::endl;
    }



}
