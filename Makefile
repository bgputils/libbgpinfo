SRCDIR = src/
PARSEDERDIR = parser/
EXAMPLESDIR = examples/
TEMPLIBDIR = lib/
TEMPBINDIR = bin/

all: lib parser examples

	
lib: prep
	$(MAKE) -C $(SRCDIR)  all

parser: prep
	$(MAKE) -C $(PARSEDERDIR)  all

examples: prep
	$(MAKE) -C $(EXAMPLESDIR)  all
	
prep:
	mkdir -p $(TEMPLIBDIR)
	mkdir -p $(TEMPBINDIR)


install:
	$(MAKE) -C $(SRCDIR)  install
	$(MAKE) -C $(PARSEDERDIR)  install
	$(MAKE) -C $(EXAMPLESDIR)  install
	
	
clean:
	$(MAKE) -C $(SRCDIR)  clean
	$(MAKE) -C $(PARSEDERDIR)  clean
	$(MAKE) -C $(EXAMPLESDIR)  clean

