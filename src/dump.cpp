#include <stdexcept>
#include <algorithm>
#include <iostream>    
#include <fstream>
#include <stdio.h>
#include <sys/stat.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>
#include <Poco/DirectoryIterator.h>

#include "dump.h"
#include "util.h"
#include "config.h"
#include "filenotfoundexception.h"

BGPInfo::Dump::Dump() {
}

BGPInfo::Dump::Dump(std::string name, time_t timestamp) {
    _name = name;
    _timestamp = timestamp;
}

BGPInfo::Dump::Dump(BGPInfo::PB::Dump dumpPB)
: BGPInfo::Dump(dumpPB.name(), dumpPB.timestamp()) {

}

std::string BGPInfo::Dump::getName() const {
    return this->_name;
}

time_t BGPInfo::Dump::getTimestamp() const {
    return this->_timestamp;
}

std::string BGPInfo::Dump::getPath() const {
    return this->_path;
}

bool BGPInfo::Dump::setPath(std::string path, bool force) {
    if (BGPInfo::Util::isDir(path)) {
        if (force) {
            this->_path = path;
        } else {
            char timestamp[20];
            struct tm *tminfo;
            tminfo = gmtime(&this->_timestamp);
            strftime(timestamp, sizeof (timestamp), "%Y%m%d_%H%M/", tminfo);

            //Create full target folder name
            this->_path = BGPInfo::Util::sanitizeDirectoryPath(path) + timestamp
                    + this->_name + BGPInfo::Config::kPathSeparator;
        }
        return true;
    }
    return false;
}

u_int32_t BGPInfo::Dump::getPrefixCount() const {
    return this->_prefixes.size();
}

u_int32_t BGPInfo::Dump::getPeerCount() const {
    return this->_peers.size();
}

u_int32_t BGPInfo::Dump::getASCount() const {
    return this->_asRelations.size();
}

u_int32_t BGPInfo::Dump::getRouteCount() const {
    return this->_routes.size();
}

std::vector<BGPInfo::Peer> BGPInfo::Dump::getPeers() const {
    return this->_peers;
}

std::set<BGPInfo::Prefix> BGPInfo::Dump::getPrefixes() const {
    return this->_prefixes;
}

std::map<uint32_t, BGPInfo::ASRelation> BGPInfo::Dump::getASRelations() const {
    return this->_asRelations;
}

std::map< Poco::Net::IPAddress, std::vector<BGPInfo::Route>> BGPInfo::Dump::getRoutes() const {
    return this->_routes;
}

void BGPInfo::Dump::addPeer(const BGPInfo::Peer peer) {
    _peers.emplace_back(peer);
}

void BGPInfo::Dump::addPeer(const std::vector<BGPInfo::Peer> peers) {
    _peers.insert(_peers.end(), peers.begin(), peers.end());
}

void BGPInfo::Dump::addPrefix(const BGPInfo::Prefix prefix) {
    _prefixes.insert(prefix);
}

void BGPInfo::Dump::addPrefix(const std::set<BGPInfo::Prefix> prefixes) {
    _prefixes.insert(prefixes.begin(), prefixes.end());
}

void BGPInfo::Dump::addRoute(const BGPInfo::Route route) {
    //determine the correct ip mask to add the route to
    int mask = BGPInfo::Config::kRoutesSplitMaskIPv4;
    Poco::Net::IPAddress net = route.getPrefix().getPrefix();
    if (net.family() == Poco::Net::IPAddress::Family::IPv6) {
        mask = BGPInfo::Config::kRoutesSplitMaskIPv6;
    }
    Poco::Net::IPAddress masked = BGPInfo::Util::maskIP(net, mask);
    _routes[masked].emplace_back(route);
}

void BGPInfo::Dump::addRoute(const std::map< Poco::Net::IPAddress
        , std::vector<BGPInfo::Route>> routes) {
    for (auto const &route : routes) {
        _routes[route.first].insert(_routes[route.first].end(), route.second.begin()
                , route.second.end());
    }


}

void BGPInfo::Dump::setASRelations(const std::map<uint32_t, BGPInfo::ASRelation> &asRelations) {
    _asRelations = asRelations;
}

void BGPInfo::Dump::setPeers(const std::vector<BGPInfo::Peer> &peers) {
    _peers = peers;
}

void BGPInfo::Dump::setPrefixes(const std::set<BGPInfo::Prefix> &prefixes) {
    _prefixes = prefixes;
}

void BGPInfo::Dump::setRoutes(const std::map< Poco::Net::IPAddress
        , std::vector<BGPInfo::Route>> &routes) {
    _routes = routes;
}

void BGPInfo::Dump::toPB(BGPInfo::PB::Dump* dump) const {
    dump->set_name(this->_name);
    dump->set_timestamp(this->_timestamp);
}

bool BGPInfo::Dump::save(bool saveAll) {
    BGPInfo::PB::Dump pbdump;
    this->toPB(&pbdump);
    Util::writeProtoBufToFile(pbdump, this->_path + BGPInfo::Config::kDumpFilename);
    if (saveAll) {
        this->saveASRelations();
        this->savePeers();
        this->savePrefixes();
        this->saveRoutes(false);
    }
    return true;
}

BGPInfo::Dump BGPInfo::Dump::load(std::string filePath, bool loadAll) {
    BGPInfo::PB::Dump pbDump;

    int fd = open(filePath.c_str(), O_RDONLY);
    if (fd == -1) {
        throw BGPInfo::Util::FileNotFoundException(filePath);
    }
    google::protobuf::io::FileInputStream fis(fd);
    google::protobuf::io::GzipInputStream gis(&fis);
    google::protobuf::io::CodedInputStream cis(&gis);
    int buffSize = 1073741824;
    cis.SetTotalBytesLimit(buffSize, buffSize - 100000);
    {
        if (!pbDump.ParseFromCodedStream(&cis)) {
            throw std::runtime_error("Failed to parse prefixes.");
        }
    }
    Poco::Path path(filePath);
    BGPInfo::Dump dump(pbDump);
    dump.setPath(path.parent().toString(), true);
    if (loadAll) {
        dump.loadASRelations();
        dump.loadPeers();
        dump.loadPrefixes();
        dump.loadRoutes();
    }
    return dump;
}

bool BGPInfo::Dump::saveASRelations() {
    if (this->_path.empty()) {
        return false;
    }

    BGPInfo::ASRelation::saveASRelations(this->_asRelations
            , this->_path + BGPInfo::Config::kASRelationFilename);
    return true;
}

bool BGPInfo::Dump::loadASRelations() {

    if (this->_path.empty()) {
        return false;
    }

    this->setASRelations(BGPInfo::ASRelation::loadASRelations(
            this->_path + BGPInfo::Config::kASRelationFilename));
    return true;
}

bool BGPInfo::Dump::savePeers() {
    if (this->_path.empty()) {
        return false;
    }

    BGPInfo::Peer::savePeers(this->_peers
            , this->_path + BGPInfo::Config::kPeersFilename);
    return true;
}

bool BGPInfo::Dump::loadPeers() {
    if (this->_path.empty()) {
        return false;
    }

    this->setPeers(BGPInfo::Peer::loadPeers(
            this->_path + BGPInfo::Config::kPeersFilename));
    return true;
}

bool BGPInfo::Dump::savePrefixes() {
    if (this->_path.empty()) {
        return false;
    }

    BGPInfo::Prefix::savePrefixes(this->_prefixes
            , this->_path + BGPInfo::Config::kPrefixesFilename);
    return true;
}

bool BGPInfo::Dump::loadPrefixes() {
    if (this->_path.empty()) {
        return false;
    }
    this->setPrefixes(BGPInfo::Prefix::loadPrefixes(
            this->_path + BGPInfo::Config::kPrefixesFilename));
    return true;
}

bool BGPInfo::Dump::saveRoutes(bool flush) {
    if (this->_path.empty()) {
        return false;
    }

    for (auto const &routeEnt : this->_routes) {
        if (routeEnt.second.size() > 0) {
            std::stringstream basePath;
            std::stringstream filePath;
            try {

                basePath << this->_path << routeEnt.first.toString()
                        << BGPInfo::Config::kPathSeparator;
                std::stringstream mkdir;
                mkdir << "mkdir -p " << basePath.str();
                const int dir_err = system(mkdir.str().c_str());
                if (-1 == dir_err) {
                    throw std::runtime_error("Error creating directory " + mkdir.str());
                }
                filePath << basePath.str() << BGPInfo::Config::kRoutesFilename;

                BGPInfo::Route::saveRoutes(routeEnt.second, filePath.str());
            } catch (char const* msg) {
                std::cout << msg << " " << filePath.str() << std::endl;
                return false;
            }
        }
    }

    if (flush) {
        this->_routes.clear();
    }

    return true;
}

bool BGPInfo::Dump::loadRoutes(bool force) {
    if (this->_path.empty()) {
        return false;
    }

    //run through all directories
    Poco::DirectoryIterator it(this->_path);
    Poco::DirectoryIterator end;
    while (it != end) {
        if (it->isDirectory() == false) {
            it++;
            continue;
        }

        std::string filepath = BGPInfo::Util::sanitizeDirectoryPath(
                it.path().toString()) + BGPInfo::Config::kRoutesFilename;

        //skip if no routes file exists
        if (BGPInfo::Util::fileExists(filepath) == false) {
            continue;
        }

        Poco::Net::IPAddress masked(it.name());
        //determine if route has already been loaded
        if (this->_alreadyLoadedRoutes.count(masked) > 0
                && force == false) {
            continue;
        }

        //        add route
        this->addRoute({
            {masked, BGPInfo::Route::loadRoutes(filepath)}
        });

        it++;
    }
    return true;
}

bool BGPInfo::Dump::loadRoutes(const BGPInfo::Prefix prefix, bool force) {
    if (this->_path.empty()) {
        return false;
    }

    //apply mask to prefix to determine the correct directory to load from
    int mask = BGPInfo::Config::kRoutesSplitMaskIPv4;
    if (prefix.getPrefix().family() == Poco::Net::IPAddress::Family::IPv6) {
        mask = BGPInfo::Config::kRoutesSplitMaskIPv6;
    }
    Poco::Net::IPAddress masked = BGPInfo::Util::maskIP(prefix.getPrefix(), mask);

    //determine if route has already been loaded
    if (this->_alreadyLoadedRoutes.count(masked) > 0 && force == false) {
        return true;
    }


    //construct the file path to the routes file
    std::string filepath = this->_path + masked.toString() +
            BGPInfo::Config::kPathSeparator + BGPInfo::Config::kRoutesFilename;

    //check if file exists
    if (BGPInfo::Util::fileExists(filepath) == false) {
        return false;
    }
    std::vector<BGPInfo::Route> routes = BGPInfo::Route::loadRoutes(filepath);


    //add route
    this->addRoute({
        {masked, BGPInfo::Route::loadRoutes(filepath)}
    });

    return true;
}

bool BGPInfo::Dump::operator!=(const Dump& a) const {
    return this->_name.compare(a._name) != 0;
}

bool BGPInfo::Dump::operator<(const Dump& a) const {
    return this->_name.compare(a._name) < 0;
}

bool BGPInfo::Dump::operator<=(const Dump& a) const {
    return this->_name.compare(a._name) <= 0;
}

bool BGPInfo::Dump::operator==(const Dump& a) const {
    return this->_name.compare(a._name) == 0;
}

bool BGPInfo::Dump::operator>(const Dump& a) const {
    return this->_name.compare(a._name) > 0;
}

bool BGPInfo::Dump::operator>=(const Dump& a) const {
    return this->_name.compare(a._name) >= 0;
}

