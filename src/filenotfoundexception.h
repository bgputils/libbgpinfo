/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   filenotfoundexception.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on November 5, 2017, 9:10 AM
 */

#ifndef FILENOTFOUNDEXCEPTION_H
#define FILENOTFOUNDEXCEPTION_H
#include <string>
#include <stdexcept>


namespace BGPInfo {
    namespace Util {

        /**
         * This exception is thrown when a mandatory file was not found.
         */
        class FileNotFoundException : public std::runtime_error {
        public:

            FileNotFoundException() : runtime_error("File not found") {
            }

            FileNotFoundException(const std::string &msg) : runtime_error(msg.c_str()) {
            }
        };

    };
};


#endif /* FILENOTFOUNDEXCEPTION_H */

