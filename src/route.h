#ifndef PATH_H
#define PATH_H

#include <Poco/Net/IPAddress.h>
#include "peer.h"
#include "bgpinfo.pb.h"
#include "prefix.h"

namespace BGPInfo {

    /**
     * Represent a BGP route. It holds a reference (ID) to the Peer it originated 
     * from, the path to the target AS, path preferences from the Peer or from 
     * nodes on the path, and other routing information. It further holds an 
     * instance of Prefix which represents the prefix this route belongs to. 
     * As not all members of this class are mandatory BGP Route/Path fields, 
     * some might hold only a default or empty value. 
     * 
     * https://tools.ietf.org/html/rfc4271#section-5.1
     */
    class Route {
    public:
        Route();

        /**
         * Constructor which takes all possible fields.
         * @param peerId BGP peer ID
         * @param origin BGP origin
         * @param timeOriginated BGP route time originated
         * @param path BGP path
         * @param nextHop BGP next hop
         * @param med BGP med option
         * @param localPref BGP route local preferences
         * @param atomicAggregate Atomic aggregate present
         * @param aggregatorIP BGP aggregator IP
         * @param aggregatorAS BGP aggregator AS
         * @param community BGP community
         * @param ecommunity BGP extended community
         * @param lcommunity BGP large community
         * @param prefix BGP prefix
         */
        Route(Poco::Net::IPAddress peerId, int origin, u_int32_t timeOriginated
                , std::string path
                , Poco::Net::IPAddress nextHop
                , u_int32_t med
                , u_int32_t localPref
                , bool atomicAggregate
                , std::string community
                , std::string ecommunity
                , std::string lcommunity
                , BGPInfo::Prefix prefix);

        /**
         * Constructor which parses a serialized Route, in protobuf message format,
         * into the actual Route class. 
         * @param routePB  Route in Protocol Buffer format
         */
        Route(BGPInfo::PB::Route routePB);
        virtual ~Route();

        /**
         * Local preferences of the Peer. It is an indicator which path the
         * Peer prefers to certain AS, if multiple paths are available.
         * @return The local preference of the Peer for this route
         */
        u_int32_t getLocalPref();
        
        /**
         * The BGP MED (Multi Exit Discriminator). Another metric to indicate a
         * preference of one path over another.
         * @return The MED of the Peer for this route
         */
        u_int32_t getMed();
        
        /**
         * Time when the path was first seen by the Peer.
         * @return Returns a timestamp in epoch time
         */
        u_int32_t getTimeOriginated();
        
        /**
         * The IP address of the next hop for this path as seen by the peer.
         * @return The next hop
         */
        Poco::Net::IPAddress getNextHop();
        
        /**
         * The origin code of this route as integer. 
         * 0 - IGP
         * 1 - EGP
         * else incomplete
         * @return The origin code
         */
        int getOrigin();
        
        /**
         * The AS path
         * @return a complete AS path list including AS-set information
         */
        std::string getPath();
        
        /**
         * Atomic aggregate attribute. Indicates if a route aggregation has happened
         * on the path.
         * @return 
         */
        bool isAtomicAggregate();
        
        /**
         * The ID of the peer
         * @return The peer ID as IP address
         */
        Poco::Net::IPAddress getPeerId();
        
        /**
         * The BGP community.An optional attribute to indicate prefixes which 
         * share some common property.
         * @return the community string
         */
        std::string getCommunity() const;
        
        /**
         * BGP large community. 
         * http://largebgpcommunities.net/
         * @return returns the large community string
         */
        std::string getLcommunity() const;
        
        /**
         * BGP extended community.
         * https://tools.ietf.org/html/rfc4360
         * @return returns the extended community string
         */
        std::string getEcommunity() const;
        
        /**
         * The prefix this route belongs to.
         * @return The prefix
         */
        BGPInfo::Prefix getPrefix() const;

        /**
         * Converts the Route to its Protobuf equivalent for serialization
         * @param route
         */
        void toPB(BGPInfo::PB::Route* route) const;

        /**
         * Converts Route vector set to Protobuf RouteList
         * @param routes
         * @return 
         */
        static BGPInfo::PB::RouteList routesToPB(std::vector<BGPInfo::Route> routes);

        /**
         * Converts Protobuf RouteList to Route vector 
         * @param pbRouteList
         * @return 
         */
        static std::vector<BGPInfo::Route> pbToRouteList(BGPInfo::PB::RouteList pbRouteList);

        /**
         * write Route vector to file
         * @param routes
         * @param filePath
         */
        static void saveRoutes(std::vector<BGPInfo::Route> routes, std::string filePath);

        /**
         * loads Route vector from file
         * @param filePath
         * @return 
         */
        static std::vector<BGPInfo::Route> loadRoutes(std::string filePath);

        bool operator!=(const Route& a) const;
        bool operator<(const Route& a) const;
        bool operator<=(const Route& a) const;
        bool operator==(const Route& a) const;
        bool operator>(const Route& a) const;
        bool operator>=(const Route& a) const;

    private:
        int _origin; //BGP origin
        uint32_t _timeOriginated; //BGP route time originated
        std::string _path; //BGP path
        Poco::Net::IPAddress _nextHop; //BGP next hop
        bool _atomicAggregate; //atomic aggregate present
        u_int32_t _med; //BGP med option
        u_int32_t _localPref; //BGP route local preferences
        Poco::Net::IPAddress _peerId; //BGP peer ID
        BGPInfo::Prefix _prefix; //BGP prefix
        std::string _community; //BGP community
        std::string _ecommunity; //BGP extended community
        std::string _lcommunity; //BGP large community
    };
}

#endif /* PATH_H */

