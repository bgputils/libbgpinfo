#ifndef PEER_H
#define PEER_H

#include <Poco/Net/IPAddressImpl.h>
#include <Poco/Net/IPAddress.h>

#include "bgpinfo.pb.h"

namespace BGPInfo {

    /**
     * Represents a BGP routing peer. It holds information about the peer router.
     */
    class Peer {
    public:
        typedef std::vector<BGPInfo::Peer> peerVector;
        Peer();

        /**
         * Constructor for the Peer. It takes all mandatory fields needed.
         * @param ip - IP of the BGP Peer 
         * @param bgpId - Peer BGP ID
         * @param as - number of the Autonomous System this Peer belongs to
         */
        Peer(Poco::Net::IPAddress ip, Poco::Net::IPAddress bgpId, uint32_t as);

        /**
         * Constructor which parses a serialized Peer, in protobuf message format,
         * into the actual Peer class. 
         * @param peerPB - Peer in Protocol Buffer format 
         */
        Peer(BGPInfo::PB::Peer peerPB);

        /**
         * The AS this Peer belongs to
         * @return The AS as integer
         */
        uint32_t GetAs();
        
        /**
         * Returns the BGP Id of the Peer
         * @return The BGP ID as Poco::Net::IPAddress
         */
        Poco::Net::IPAddress GetBgpId();
        Poco::Net::IPAddress GetIp();

        /**
         * Converts the Peer to its Protobuf equivalent for serialization
         * @param p
         */
        void toPB(BGPInfo::PB::Peer* p) const;

        /**
         * Converts Peer vector to Protobuf PeerList
         * @param peers
         * @return 
         */
        static BGPInfo::PB::PeerList PeersToPB(std::vector<BGPInfo::Peer> peers);

        /**
         * Converts Protobuf PeerList to Peer vector 
         * @param pbPeerList
         * @return 
         */
        static std::vector<BGPInfo::Peer> pbToPeers(BGPInfo::PB::PeerList pbPeerList);

        /**
         * save Peer list to file
         * @param peers
         * @param filePath
         */
        static void savePeers(std::vector<BGPInfo::Peer> peers, std::string filePath);

        /**
         * loads Peer list from file
         * @param filePath
         * @return 
         */
        static std::vector<BGPInfo::Peer> loadPeers(std::string filePath);

        bool operator!=(const Peer& a) const;
        bool operator<(const Peer& a) const;
        bool operator<=(const Peer& a) const;
        bool operator==(const Peer& a) const;
        bool operator>(const Peer& a) const;
        bool operator>=(const Peer& a) const;


    private:
        Poco::Net::IPAddress _ip; //peer IP
        Poco::Net::IPAddress _bgpId; //peer BGP ID
        uint32_t _as; //peer BGP AS

    };
}
#endif /* PEER_H */

