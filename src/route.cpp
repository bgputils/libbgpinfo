/* 
 * File:   route.cpp
 * Author: root
 * 
 * Created on January 27, 2017, 11:28 AM
 */

#include "route.h"
#include <iostream>    
#include <fstream>
#include <stdio.h>
#include <sys/stat.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>
#include <stdlib.h>

#include "util.h"
#include "filenotfoundexception.h"

BGPInfo::Route::Route() {
}

BGPInfo::Route::~Route() {
}

BGPInfo::Route::Route(Poco::Net::IPAddress peerId, int origin, u_int32_t timeOriginated, std::string path
        , Poco::Net::IPAddress nextHop
        , u_int32_t med
        , u_int32_t localPref
        , bool atomicAggregate
        , std::string community
        , std::string ecommunity
        , std::string lcommunity
        , BGPInfo::Prefix prefix) {
    _peerId = peerId;
    _origin = origin;
    _timeOriginated = timeOriginated;
    _path = path;
    _nextHop = nextHop;
    _med = med;
    _localPref = localPref;
    _atomicAggregate = atomicAggregate;
    _community = community;
    _ecommunity = ecommunity;
    _lcommunity = lcommunity;
    _prefix = prefix;
}

BGPInfo::Route::Route(BGPInfo::PB::Route routePB)
: BGPInfo::Route(Poco::Net::IPAddress::parse(routePB.peerid())
, routePB.origin()
, routePB.timeoriginated()
, routePB.path()
, Poco::Net::IPAddress::parse(routePB.nexthop())
, routePB.med()
, routePB.localpref()
, routePB.atomicaggregate()
, routePB.community()
, routePB.ecommunity()
, routePB.lcommunity()
, BGPInfo::Prefix(routePB.prefix())) {
}

u_int32_t BGPInfo::Route::getLocalPref() {
    return _localPref;
}

u_int32_t BGPInfo::Route::getMed() {
    return _med;
}

Poco::Net::IPAddress BGPInfo::Route::getNextHop() {
    return _nextHop;
}

int BGPInfo::Route::getOrigin() {
    return _origin;
}

u_int32_t BGPInfo::Route::getTimeOriginated() {
    return _timeOriginated;
}

std::string BGPInfo::Route::getPath() {
    return _path;
}

bool BGPInfo::Route::isAtomicAggregate() {
    return _atomicAggregate;
}

Poco::Net::IPAddress BGPInfo::Route::getPeerId() {
    return _peerId;
}

BGPInfo::Prefix BGPInfo::Route::getPrefix() const {
    return _prefix;
}

std::string BGPInfo::Route::getCommunity() const {
    return _community;
}

std::string BGPInfo::Route::getLcommunity() const {
    return _lcommunity;
}

std::string BGPInfo::Route::getEcommunity() const {
    return _ecommunity;
}

void BGPInfo::Route::toPB(BGPInfo::PB::Route* r) const {
    r->set_origin(this->_origin);
    r->set_path(this->_path);
    r->set_nexthop(this->_nextHop.toString());
    r->set_atomicaggregate(this->_atomicAggregate);
    r->set_med(this->_med);
    r->set_localpref(this->_localPref);
    r->set_peerid(this->_peerId.toString());
    r->set_timeoriginated(this->_timeOriginated);
    r->set_community(this->_community);
    r->set_ecommunity(this->_ecommunity);
    r->set_lcommunity(this->_lcommunity);
    BGPInfo::PB::Prefix *bpprefix = new BGPInfo::PB::Prefix();
    this->_prefix.toPB(bpprefix);
    r->set_allocated_prefix(bpprefix);

}

BGPInfo::PB::RouteList BGPInfo::Route::routesToPB(std::vector<BGPInfo::Route> routes) {

    BGPInfo::PB::RouteList pbRouteList;
    for (auto& route : routes) {
        route.toPB(pbRouteList.add_routes());
    }

    return pbRouteList;
}

std::vector<BGPInfo::Route> BGPInfo::Route::pbToRouteList(BGPInfo::PB::RouteList pbRouteList) {
    std::vector<BGPInfo::Route> routes;
    for (int i = 0; i < pbRouteList.routes_size(); i++) {
        const BGPInfo::PB::Route& pbroute = pbRouteList.routes(i);
        BGPInfo::Route route = BGPInfo::Route(pbroute);
        routes.push_back(route);
    }
    return routes;
}

void BGPInfo::Route::saveRoutes(std::vector<BGPInfo::Route> routes, std::string filePath) {
    BGPInfo::PB::RouteList pbRouteList = routesToPB(routes);
    Util::writeProtoBufToFile(pbRouteList, filePath);
}

std::vector<BGPInfo::Route> BGPInfo::Route::loadRoutes(std::string filePath) {
    BGPInfo::PB::RouteList routes;
    int fd = open(filePath.c_str(), O_RDONLY);
    if (fd == -1) {
        throw BGPInfo::Util::FileNotFoundException(filePath);
    }
    google::protobuf::io::FileInputStream fis(fd);
    google::protobuf::io::GzipInputStream gis(&fis);
    google::protobuf::io::CodedInputStream cis(&gis);
    int buffSize = 1073741824;
    cis.SetTotalBytesLimit(buffSize, buffSize - 100000);
    {
        if (!routes.ParseFromCodedStream(&cis)) {
            throw std::runtime_error("Failed to parse prefixes.");
        }
    }

    return pbToRouteList(routes);
}

bool BGPInfo::Route::operator!=(const Route& a) const {

    return this->_path.compare(a._path) != 0 || this->_prefix != a.getPrefix();
}

bool BGPInfo::Route::operator<(const Route& a) const {
    return this->_path.compare(a._path) < 0 && this->_prefix < a.getPrefix();
}

bool BGPInfo::Route::operator<=(const Route& a) const {
    return this->_path.compare(a._path) <= 0 && this->_prefix <= a.getPrefix();
}

bool BGPInfo::Route::operator==(const Route& a) const {
    return this->_path.compare(a._path) == 0 && this->_prefix == a.getPrefix();
}

bool BGPInfo::Route::operator>(const Route& a) const {
    return this->_path.compare(a._path) > 0 && this->_prefix > a.getPrefix();
}

bool BGPInfo::Route::operator>=(const Route& a) const {
    return this->_path.compare(a._path) >= 0 && this->_prefix >= a.getPrefix();
}
