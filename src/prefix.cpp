#include <algorithm>
#include <iostream>    
#include <stdio.h>
#include <sys/stat.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>

#include "prefix.h"
#include "util.h"
#include "filenotfoundexception.h"

using namespace Poco::Net;

BGPInfo::Prefix::Prefix() {
}

BGPInfo::Prefix::~Prefix() {
}

BGPInfo::Prefix::Prefix(in_addr* prefix, int prefixLength, std::set<uint32_t> asList)
: BGPInfo::Prefix(Util::parseIP(*prefix), prefixLength, asList) {
}

BGPInfo::Prefix::Prefix(in6_addr* prefix, int prefixLength, std::set<uint32_t> asList)
: BGPInfo::Prefix(Util::parseIP(*prefix), prefixLength, asList) {
}

BGPInfo::Prefix::Prefix(in_addr* prefix, int prefixLength)
: BGPInfo::Prefix(Util::parseIP(*prefix), prefixLength) {
}

BGPInfo::Prefix::Prefix(in6_addr* prefix, int prefixLength)
: BGPInfo::Prefix(Util::parseIP(*prefix), prefixLength) {
}

BGPInfo::Prefix::Prefix(Poco::Net::IPAddress prefix, int prefixLength
        , std::set<uint32_t> asList) {
    _prefix = prefix;
    this->setLength(prefixLength);
    _asList = asList;
}

BGPInfo::Prefix::Prefix(Poco::Net::IPAddress prefix, int prefixLength) {
    _prefix = prefix;
    this->setLength(prefixLength);
}

BGPInfo::Prefix::Prefix(BGPInfo::PB::Prefix prefixPB)
: BGPInfo::Prefix(Poco::Net::IPAddress::parse(prefixPB.prefix()), prefixPB.length()
, BGPInfo::Util::parseBPRepeatedFieldtoSet(prefixPB.asn())) {
}

int BGPInfo::Prefix::getLength() const {
    return _length;
}

void BGPInfo::Prefix::setLength(int length) {
    //some sanity check
    if ((_prefix.family() == Poco::Net::IPAddress::Family::IPv6
            && (length < 0 || length > 128))
            || (_prefix.family() == Poco::Net::IPAddress::Family::IPv4
            && (length < 0 || length > 32))) {
        throw Poco::InvalidArgumentException("Invalid prefix length supplied [0,(32,128)]");
    }

    _length = length;

}

Poco::Net::IPAddress BGPInfo::Prefix::getPrefix() const {
    return _prefix;
}

void BGPInfo::Prefix::addASN(int asn) {
    _asList.insert(asn);
}

std::set<uint32_t> BGPInfo::Prefix::getASList() const {
    return _asList;
}

void BGPInfo::Prefix::toPB(BGPInfo::PB::Prefix* p) const {
    p->set_prefix(this->_prefix.toString());
    p->set_length(this->_length);
    for (auto const& value : this->_asList) {
        p->add_asn(value);
    }
}

BGPInfo::PB::PrefixList BGPInfo::Prefix::prefixesToPB(std::set<BGPInfo::Prefix> prefixes) {

    BGPInfo::PB::PrefixList pbPrefixList;
    for (auto& prefix : prefixes) {
        prefix.toPB(pbPrefixList.add_prefixes());
    }
    return pbPrefixList;
}

std::set<BGPInfo::Prefix> BGPInfo::Prefix::pbToPrefixList(BGPInfo::PB::PrefixList pbPrefixList) {
    std::set<BGPInfo::Prefix> prefixes;
    for (int i = 0; i < pbPrefixList.prefixes_size(); i++) {
        const BGPInfo::PB::Prefix& pbprefix = pbPrefixList.prefixes(i);
        BGPInfo::Prefix prefix = BGPInfo::Prefix(pbprefix);
        prefixes.insert(prefix);
    }
    return prefixes;
}

void BGPInfo::Prefix::savePrefixes(std::set<BGPInfo::Prefix> prefixes, std::string filePath) {
    BGPInfo::PB::PrefixList pbPrefixList = prefixesToPB(prefixes);
    Util::writeProtoBufToFile(pbPrefixList, filePath);
}

std::set<BGPInfo::Prefix> BGPInfo::Prefix::loadPrefixes(std::string filePath) {
    BGPInfo::PB::PrefixList prefixes;
    int fd = open(filePath.c_str(), O_RDONLY);
    if (fd == -1) {
        throw BGPInfo::Util::FileNotFoundException(filePath);
    }
    google::protobuf::io::FileInputStream fis(fd);
    google::protobuf::io::GzipInputStream gis(&fis);
    google::protobuf::io::CodedInputStream cis(&gis);
    int buffSize = 1073741824;
    cis.SetTotalBytesLimit(buffSize, buffSize - 100000);
    {
        if (!prefixes.ParseFromCodedStream(&cis)) {
            throw std::runtime_error("Failed to parse prefixes.");
        }
    }

    return pbToPrefixList(prefixes);
}

bool BGPInfo::Prefix::operator!=(const Prefix& a) const {
    return this->_prefix != a._prefix || this->_length != a._length;
}

bool BGPInfo::Prefix::operator<(const Prefix& a) const {
    if (this->_prefix == a._prefix) {
        if (this->_length == a._length) {
            return false;
        }
        return this->_length < a._length;
    } else {
        return this->_prefix < a._prefix;
    }
}

bool BGPInfo::Prefix::operator<=(const Prefix& a) const {
    if (this->_prefix == a._prefix) {
        if (this->_length == a._length) {
            return false;
        }
        return this->_length <= a._length;
    } else {
        return this->_prefix <= a._prefix;
    }
}

bool BGPInfo::Prefix::operator==(const Prefix& a) const {
    return this->_prefix == a._prefix && this->_length == a._length && this->_length == a._length;
}

bool BGPInfo::Prefix::operator>(const Prefix& a) const {
    if (this->_prefix == a._prefix) {
        if (this->_length == a._length) {
            return false;
        }
        return this->_length > a._length;
    } else {
        return this->_prefix > a._prefix;
    }
}

bool BGPInfo::Prefix::operator>=(const Prefix& a) const {
    if (this->_prefix == a._prefix) {
        if (this->_length == a._length) {
            return false;
        }
        return this->_length >= a._length;
    } else {
        return this->_prefix >= a._prefix;
    }
}

