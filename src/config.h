#ifndef CONFIG_H
#define CONFIG_H
namespace BGPInfo {
    namespace Config {
        
        /**
         * Names for the dump files
         */
        const std::string kDumpFilename = "dump.pb";
        const std::string kASRelationFilename = "asrelations.pb";
        const std::string kPeersFilename = "peers.pb";
        const std::string kRoutesFilename = "routes.pb";
        const std::string kPrefixesFilename = "prefixes.pb";
        
        const std::string kPathSeparator = "/";


        /*
         * Defines the CIDR netmask that is used to split up the routes in separate 
         * folders. For example would 32.25.5.4 be part of 32.0.0.0 routes 
         * (with mask =  8).
         * 
         * If this value is changed correct reading of pre-change dumps is not
         * guaranteed
         */
        const int kRoutesSplitMaskIPv4 = 8;
        const int kRoutesSplitMaskIPv6 = 8;
    }
}


#endif /* CONFIG_H */

