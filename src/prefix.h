#ifndef PREFIX_H
#define PREFIX_H

#include <Poco/Net/IPAddress.h>
#include <Poco/Exception.h>
#include <Poco/BinaryWriter.h>
#include <Poco/BinaryReader.h>
#include "bgpinfo.pb.h"

#include <vector>
#include <unordered_set>

#define ADDRSTRLEN 46


namespace BGPInfo {

    /**
     * This class represents a BGP prefix (Net IP + length) for either IPv4 or
     * IPv6 networks. Further it holds a list of Autonomous System Numbers (ASN)
     * in which the prefix was originated. This is usually not part of a Prefix
     * but it allows faster search for multi homed Prefixes to perform Anycast IP or 
     * Prefix spoofing detection.
     */
    class Prefix {
    public:

        /**
         * Constructor for IPv4 prefixes in integer format which takes a list
         * of ASes where this prefix was originated.
         * @param prefix - Network prefix in binary network format
         * @param prefixLength - Network prefix length
         * @param asn - number of the Autonomous System this prefix belongs to
         */
        Prefix(in_addr* prefix, int prefixLength, std::set<uint32_t> asList);

        /**
         * Constructor for IPv4 prefixes in integer format. 
         * @param prefix - Network prefix in binary network format
         * @param prefixLength - Network prefix length
         */
        Prefix(in_addr* prefix, int prefixLength);

        /**
         * Constructor for IPv6 prefixes in integer format which takes a list
         * of ASes where this prefix has originated.
         * @param prefix - Network prefix in binary network format
         * @param prefixLength - Network prefix length
         * @param asn - number of the Autonomous System this prefix belongs to
         */
        Prefix(in6_addr* prefix, int prefixLength, std::set<uint32_t> asList);

        /**
         * Constructor for IPv4 prefixes in integer format.
         * @param prefix - Network prefix in binary network format
         * @param prefixLength - Network prefix length         */
        Prefix(in6_addr* prefix, int prefixLength);


        /**
         * Constructor for IPv4 and IPv6 prefixes as POCO::NET:IPAddress which 
         * takes a list of ASes where this prefix  has originated.
         * @param prefix - Network prefix 
         * @param prefixLength -  Network prefix length
         * @param asn - number of the Autonomous System this prefix belongs to
         */
        Prefix(Poco::Net::IPAddress prefix, int prefixLength, std::set<uint32_t> asList);


        /**
         * Constructor for IPv4 and IPv6 prefixes as POCO::NET:IPAddress.
         * @param prefix - Network prefix 
         * @param prefixLength -  Network prefix length
         */
        Prefix(Poco::Net::IPAddress prefix, int prefixLength);


        /**
         * Constructor which parses a serialized Prefix, in protobuf message format,
         * into the actual Prefix class. 
         * @param prefixPB - Prefix in Protocol Buffer format  
         */
        Prefix(BGPInfo::PB::Prefix prefixPB);

        Prefix();
        virtual ~Prefix();

        /**
         * The prefix length
         * @return Returns the prefix length as integer
         */
        int getLength() const;

        /**
         * The Prefix
         * @return The prefix IP 
         */
        Poco::Net::IPAddress getPrefix() const;

        /**
         * Add AS to the AS list
         * @param asn
         */
        void addASN(const int asn);

        /**
         * The complete list of AS where this Prefix originated in
         * @return 
         */
        std::set<uint32_t> getASList() const;

        /**
         * Converts the Prefix to its Protobuf equivalent for serialization
         * @param p
         */
        void toPB(BGPInfo::PB::Prefix* p) const;

        /**
         * Converts Prefixes set to Protobuf PrefixList
         * @param prefixes
         * @return 
         */
        static BGPInfo::PB::PrefixList prefixesToPB(std::set<BGPInfo::Prefix> prefixes);

        /**
         * Converts Protobuf PrefixList to Prefixes set 
         * @param pbPrefixList
         * @return 
         */
        static std::set<BGPInfo::Prefix> pbToPrefixList(BGPInfo::PB::PrefixList pbPrefixList);

        /**
         * write Prefixes set to file
         * @param prefixes
         * @param filePath
         */
        static void savePrefixes(std::set<BGPInfo::Prefix> prefixes, std::string filePath);

        /**
         * loads Prefixes set from file
         * @param filePath
         * @return 
         */
        static std::set<BGPInfo::Prefix> loadPrefixes(std::string filePath);

        bool operator!=(const Prefix& a) const;
        bool operator<(const Prefix& a) const;
        bool operator<=(const Prefix& a) const;
        bool operator==(const Prefix& a) const;
        bool operator>(const Prefix& a) const;
        bool operator>=(const Prefix& a) const;

    private:

        Poco::Net::IPAddress _prefix; // The prefix
        int _length; // Prefix length
        std::set<uint32_t> _asList; //List of AS where this Prefix originated in

        /**
         * Sets the prefix length
         * @param length
         */
        void setLength(int length);
        
    };
}


namespace std {

    template <>
    /**
     * Template for Prefix hash function
     * @param prefix
     * @return 
     */
    struct hash<BGPInfo::Prefix> {

        std::size_t operator()(const BGPInfo::Prefix& prefix) const {
            using std::size_t;
            using std::hash;
            using std::string;

            // Compute individual hash values for prefix,
            // length and combine them using XOR
            // and bit shifting:
            return ((hash<string>()(prefix.getPrefix().toString())
                    ^ (hash<int>()(prefix.getLength()) << 1)) >> 1);
        }
    };

}

#endif /* PREFIX_H */

