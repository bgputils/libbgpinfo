#include "util.h"
#include <stdio.h>
#include <Poco/Net/NetException.h>
#include <Poco/Net/IPAddressImpl.h>
#include <fstream>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>
#include <sys/stat.h>

#define ADDRSTRLEN 46

void BGPInfo::Util::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {

        elems.push_back(item);
    }
}

std::vector<std::string> BGPInfo::Util::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);

    return elems;
}

std::vector<std::string> BGPInfo::Util::split2(std::string s, const char delimiter) {
    size_t start = 0;
    size_t end = s.find_first_of(delimiter);

    std::vector<std::string> output;

    if (s.empty() == false) {

        while (end <= std::string::npos) {
            output.emplace_back(s.substr(start, end - start));

            if (end == std::string::npos)
                break;

            start = end + 1;
            end = s.find_first_of(delimiter, start);
        }

    }
    return output;
}

Poco::Net::IPAddress BGPInfo::Util::parseIP(BGPDUMP_IP_ADDRESS ip, int afi) {
    char cip[ADDRSTRLEN];

    if (afi == AFI_IP) {
        inet_ntop(AF_INET, &ip.v4_addr, cip, INET_ADDRSTRLEN);
    } else {
        inet_ntop(AF_INET6, &ip.v6_addr, cip, INET6_ADDRSTRLEN);
    }

    try {
        return Poco::Net::IPAddress(cip);
    } catch (const Poco::Net::InvalidAddressException& ia) {
        std::cout << "Invalid argument: " << ia.what() << " ip: " << cip << " afi: " << afi << std::endl;
    }
    return Poco::Net::IPAddress();

}

Poco::Net::IPAddress BGPInfo::Util::parseIP(in_addr ip) {
    char cip[ADDRSTRLEN];
    inet_ntop(AF_INET, &ip, cip, INET_ADDRSTRLEN);
    return Poco::Net::IPAddress(cip);
}

Poco::Net::IPAddress BGPInfo::Util::parseIP(in6_addr ip) {
    char cip[ADDRSTRLEN];
    inet_ntop(AF_INET6, &ip, cip, INET6_ADDRSTRLEN);
    return Poco::Net::IPAddress(cip);
}

Poco::Net::IPAddress BGPInfo::Util::maskIP(Poco::Net::IPAddress ip, unsigned prefixLength) {
    if (ip.family() == Poco::Net::IPAddress::IPv4) {
        Poco::Net::IPAddress mask = Poco::Net::IPAddress(prefixLength, Poco::Net::IPAddress::IPv4);
        ip.mask(mask);
    } else {
        Poco::Net::IPAddress mask = Poco::Net::IPAddress(prefixLength, Poco::Net::IPAddress::IPv6);
        ip = ip & mask;
    }
    return ip;
}

bool BGPInfo::Util::fileExists(std::string fileName) {
    std::ifstream infile(fileName);
    return infile.good();
}

bool BGPInfo::Util::isFile(std::string path) {
    struct stat buf;
    stat(path.c_str(), &buf);
    return S_ISREG(buf.st_mode);
}

bool BGPInfo::Util::isDir(const std::string path) {
    struct stat buf;
    stat(path.c_str(), &buf);
    return S_ISDIR(buf.st_mode);
}

std::string BGPInfo::Util::sanitizeDirectoryPath(std::string path) {
    if (!path.empty() && path.back() != '/')
        path += '/';
    return path;
}

void BGPInfo::Util::writeProtoBufToFile(::google::protobuf::Message& message, std::string filePath) {
    using namespace google::protobuf::io;
    int fd = open((filePath).c_str(), O_WRONLY | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);

    if (fd == -1) {
        throw "open failed on output file";
    }

    google::protobuf::io::FileOutputStream file_stream(fd);
    GzipOutputStream::Options options;
    options.format = GzipOutputStream::GZIP;
    options.compression_level = 5;
    google::protobuf::io::GzipOutputStream gzip_stream(&file_stream, options);
    if (!message.SerializeToZeroCopyStream(&gzip_stream)) {
        std::cout << "Failed to write serialize." << std::endl;
        return;
    }
    //close streams before closing the file descriptor to avoid data loss
    gzip_stream.Close();
    file_stream.Close();

    close(fd);
}
