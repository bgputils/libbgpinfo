#ifndef DUMP_H
#define DUMP_H
extern "C" {
#include "bgpdump_lib.h"
}
#include <string>
#include <unordered_set>
#include <map>

#include "peer.h"
#include "route.h"
#include "prefix.h"
#include "bgpinfo.pb.h"
#include "asrelation.h"

namespace BGPInfo {

    /**
     * This class represent a BGP Dump. It holds basic dump information like name (for 
     * MRT dumps this could be the collector name), date of creation, and path on 
     * disk. Further it holds lists for ASRelations, Peers, Prefixes and Routes.
     * Routes are treated specially as the could use up a lot of memory when loaded
     * at once. Routes are grouped into "network groups" which means all routes 
     * belonging to a prefix in 8.0.0.0/8 range are written in the same file.
     * This allows lazy loading of routes and keeps the memory footprint low.
     * Saving the dump to disk does create a folder structure which holds all 
     * separate files for the previously mentioned data. Further this class
     * defines all functions to read dump data from disk. 
     * A word of caution: Loading all data from the dump at once, when dealing 
     * with data from BGP route collectors, is not recommended as the vast amount
     * of routes could exhaust the memory.
     */
    class Dump {
    public:
        Dump();

        /**
         * 
         * @param name Dump name
         * @param timestamp Time of the dump
         */
        Dump(std::string name, time_t timestamp);

        /**
         * Converts the Dump to its Protobuf pendant for serialization
         * @param dumpPB
         */
        Dump(BGPInfo::PB::Dump dumpPB);

        std::string getName() const;
        time_t getTimestamp() const;
        std::string getPath() const;


        /**
         * Sets the path to the dump. If "force" is set true the given directory 
         * will be used for reading and writing. If force is set false a subdirectory
         * of the given path, named <dumptimestamp><dumpname> will be used.
         * @param path The path where the dump is located
         * @param force Specifies if the given path or a automatically named 
         * subdirectory should be used.
         * @return 
         */
        bool setPath(std::string path, bool force = false);

        /**
         * Number of prefixes in this dump
         * @return An unsigned integer indicating the number of prefixes in the list
         */
        u_int32_t getPrefixCount() const;

        /**
         * Number of peers in this dump
         * @return An unsigned integer indicating the number of peers in the list
         */
        u_int32_t getPeerCount() const;

        /**
         * Number of Autonomous Systems in this dump
         * @return An unsigned integer indicating the number of Autonomous Systems
         * in this dump
         */
        u_int32_t getASCount() const;

        /**
         * Number of route network groups
         * @return An unsigned integer indicating the number of route network groups
         * in this dump
         */
        u_int32_t getRouteCount() const;

        /**
         * Returns a list of peers in this dump
         * @return a std::vector containing peers. This can be empty.
         */
        std::vector<BGPInfo::Peer> getPeers() const;

        /**
         * Returns a list of prefixes in this dump
         * @return a std::set containing prefixes. This can be empty.
         */
        std::set<BGPInfo::Prefix> getPrefixes() const;

        /**
         * Returns a list of AS relations in this dump
         * @return a std::vector containing AS relations. This can be empty.
         */
        std::map<uint32_t, BGPInfo::ASRelation> getASRelations() const;

        /**
         * Returns a list of routes in this dump
         * @return a std::vector containing routes. This can be empty
         */
        std::map< Poco::Net::IPAddress, std::vector<BGPInfo::Route>> getRoutes() const;

        /**
         * Converts the Dump to its Protobuf equivalent for serialization.
         * @param p
         */
        void toPB(BGPInfo::PB::Dump* dump) const;

        /**
         * Adds a peer to the peer list
         * @param peer BGP Peer
         */
        void addPeer(const BGPInfo::Peer peer);

        /**
         * Adds multiple peers to the peer list
         * @param peer BGP Peer
         */
        void addPeer(const std::vector<BGPInfo::Peer> peers);

        /**
         * Adds a prefix to the prefix list
         * @param prefix BGP prefix
         */
        void addPrefix(const BGPInfo::Prefix prefix);

        /**
         * Adds multiple prefixes to the prefix list
         * @param prefixes
         */
        void addPrefix(const std::set<BGPInfo::Prefix> prefixes);

        /**
         * Adds a route to the route list
         * @param route BGP route
         */
        void addRoute(const BGPInfo::Route route);


        /**
         * Adds multiple routes to the route list
         * @param route BGP route
         */
        void addRoute(const std::map<Poco::Net::IPAddress, std::vector<BGPInfo::Route>> routes);


        /**
         * Sets the AS relation map
         * @param asrelation
         */
        void setASRelations(const std::map<uint32_t, BGPInfo::ASRelation> &asrelation);

        /**
         * Sets the the peer list
         * @param peers The peer list
         */
        void setPeers(const std::vector<BGPInfo::Peer> &peers);

        /**
         * Sets the prefix list
         * Set is used instead of a hash table (like unordered_set) to allow
         * range searches without having to repack the container. This might has
         * negative impact on exact search performance, which is accepted by now
         * as there is no, one fits all solution.
         * @param prefixes The prefix list
         */
        void setPrefixes(const std::set<BGPInfo::Prefix> &prefixes);

        /**
         * Sets the route list
         * @param routes The route list
         */
        void setRoutes(const std::map< Poco::Net::IPAddress
                , std::vector<BGPInfo::Route>> &routes);


        /**
         * Saves the Dump to file. This can either be only the dump information
         * or if "saveAll = true" all other data structures (peers, routes, prefixes, 
         * asrelations,..) too. By default only the dump information is written
         * @param filePath
         * @param saveAll Indicates if only the dump information should be written or
         * all data structures
         * @return Returns true if saving was successful, false else
         */
        bool save(bool saveAll = false);

        /**
         * Loads the Dump from file. This function by default only loads the dump 
         * information. All other data can either be loaded separately (using 
         * load loadX(), where X can be Prefix, ASRelations, Routes) as loading 
         * certain data, like routes, as bulk could lead to memory exhaustion. 
         * Using loadAll = true or loadRoutes() should therefore used with caution.
         * @param filePath
         * @param loadAll
         * @return 
         */
        static BGPInfo::Dump load(std::string filePath, bool loadAll = false);

        /**
         * Save all AS relations to disk
         * @return Returns true if saving was successful, false else
         */
        bool saveASRelations();

        /**
         * Loads all AS relations for this dump from disk. 
         * The dump path must be set, which is either done when the dump was loaded
         * from a file or has been saved to file previously.
         * @return Returns true if loading was successful, false else
         */
        bool loadASRelations();

        /**
         * Save all Peers to disk
         * @return Returns true if saving was successful, false else
         */
        bool savePeers();

        /**
         * Loads all Peers from disk
         * The dump path must be set, which is either done when the dump was loaded
         * from a file or has been saved to file previously.
         * @return Returns true if loading was successful, false else
         */
        bool loadPeers();

        /**
         * Save all Prefixes to disk
         * @return Returns true if saving was successful, false else
         */
        bool savePrefixes();

        /**
         * Loads all Prefixes from disk
         * The dump path must be set, which is either done when the dump was loaded
         * from a file or has been saved to file previously.
         * @return Returns true if loading was successful, false else
         */
        bool loadPrefixes();

        /**
         * Save all Routes to disk
         * @param flush Indicates if routes should be flushed after they have 
         * been written.
         * @return Returns true if saving was successful, false else
         */
        bool saveRoutes(bool flush = false);

        /**
         * Loads all Routes from disk. Use with caution as this could exhaust your memory.
         * The dump path must be set, which is either done when the dump was loaded
         * from a file or has been saved to file previously.
         * @param force Indicates if routes should be loaded even if they have
         * been loaded previously. Default is false;
         * @return Returns true if loading was successful, false else
         */
        bool loadRoutes(bool force = false);

        /**
         * Loads only the routes for the given prefixes. If you don't need all routes
         * for all prefixes, this should be the first option to choose.
         * @param prefix The prefix the routes should be loaded for
         * @param force Indicates if the routes should be loaded even if they
         * have been loaded already. Default is false;
         * @return Returns true if loading was successful, false else
         */
        bool loadRoutes(const BGPInfo::Prefix prefix, bool force = false);


        bool operator!=(const Dump& a) const;
        bool operator<(const Dump& a) const;
        bool operator<=(const Dump& a) const;
        bool operator==(const Dump& a) const;
        bool operator>(const Dump& a) const;
        bool operator>=(const Dump& a) const;

    private:
        std::set<Poco::Net::IPAddress> _alreadyLoadedRoutes;
        std::string _name; //dump name
        time_t _timestamp; //time of the dump
        std::string _path; //path to the libbgpinfo dump structure on disk

        std::set<BGPInfo::Prefix> _prefixes; //BGP prefixes
        std::vector<BGPInfo::Peer> _peers; //BGP peers
        std::map<uint32_t, BGPInfo::ASRelation> _asRelations; // AS relation list
        std::map< Poco::Net::IPAddress, std::vector<BGPInfo::Route>> _routes; // Route list
    };
}
#endif /* DUMP_H */

