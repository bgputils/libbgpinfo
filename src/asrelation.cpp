#include <stdexcept>
#include <algorithm>
#include <iostream>    
#include <fstream>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sstream>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>

#include "asrelation.h"
#include "util.h"
#include "filenotfoundexception.h"

BGPInfo::ASRelation::ASRelation() {
}

BGPInfo::ASRelation::ASRelation(uint32_t asn) {
    this->_left.reserve(60000);
    this->_right.reserve(60000);
    this->_asn = asn;
}

BGPInfo::ASRelation::ASRelation(BGPInfo::PB::ASRelation asRelation) {
    this->_asn = asRelation.asn();
    for (int i = 0; i < asRelation.left_size(); i++) {
        this->addLeft(asRelation.left(i));
    }
    for (int i = 0; i < asRelation.right_size(); i++) {
        this->addRight(asRelation.right(i));
    }
}

uint32_t BGPInfo::ASRelation::getAsn() const {
    return _asn;
}

std::vector<uint32_t> BGPInfo::ASRelation::getLeft() const {
    return _left;
}

std::vector<uint32_t> BGPInfo::ASRelation::getRight() const {
    return _right;
}

void BGPInfo::ASRelation::addLeft(uint32_t left) {
    if (std::find(_left.begin(), _left.end(), left) == _left.end()) {
        _left.emplace_back(left);
    }
}

void BGPInfo::ASRelation::addRight(uint32_t right) {
    if (std::find(_right.begin(), _right.end(), right) == _right.end()) {
        _right.emplace_back(right);
    }
}

void BGPInfo::ASRelation::addLeftRight(uint32_t left, uint32_t right) {
    this->addLeft(left);
    this->addRight(right);
}

void BGPInfo::ASRelation::toPB(BGPInfo::PB::ASRelation* asr) const {
    asr->set_asn(this->_asn);
    for (auto const& value : this->_left) {
        asr->add_left(value);
    }
    for (auto const& value : this->_right) {
        asr->add_right(value);
    }
}

BGPInfo::PB::ASRelationList BGPInfo::ASRelation::aSrelationsToPB(std::map<uint32_t, BGPInfo::ASRelation> asRelations) {

    BGPInfo::PB::ASRelationList pbASRelationList;
    for (auto& asRelation : asRelations) {
        asRelation.second.toPB(pbASRelationList.add_asl());
    }
    return pbASRelationList;
}

std::map<uint32_t, BGPInfo::ASRelation> BGPInfo::ASRelation::pbToASrelations(BGPInfo::PB::ASRelationList pbASRelationList) {
    std::map<uint32_t, BGPInfo::ASRelation> asRelations;
    for (int i = 0; i < pbASRelationList.asl_size(); i++) {
        const BGPInfo::PB::ASRelation& pbasrelation = pbASRelationList.asl(i);
        BGPInfo::ASRelation asRelation = BGPInfo::ASRelation(pbasrelation);
        asRelations[asRelation.getAsn()] = asRelation;
    }
    return asRelations;
}

void BGPInfo::ASRelation::saveASRelations(std::map<uint32_t, BGPInfo::ASRelation> asRelationList, std::string filePath) {
    BGPInfo::PB::ASRelationList pbASRelationList = aSrelationsToPB(asRelationList);
    Util::writeProtoBufToFile(pbASRelationList, filePath);
}

std::map<std::uint32_t, BGPInfo::ASRelation> BGPInfo::ASRelation::loadASRelations(std::string filePath) {
    BGPInfo::PB::ASRelationList pbASRelationList;
    int fd = open(filePath.c_str(), O_RDONLY);
    if (fd == -1) {
        throw BGPInfo::Util::FileNotFoundException(filePath);
    }
    google::protobuf::io::FileInputStream fis(fd);
    google::protobuf::io::GzipInputStream gis(&fis);
    google::protobuf::io::CodedInputStream cis(&gis);
    int buffSize = 1073741824;
    cis.SetTotalBytesLimit(buffSize, buffSize - 100000);
    {
        if (!pbASRelationList.ParseFromCodedStream(&cis)) {
            throw std::runtime_error("Failed to parse prefixes.");
        }
    }

    return pbToASrelations(pbASRelationList);
}
