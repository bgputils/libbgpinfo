#ifndef ASNRELATION_H
#define ASNRELATION_H
#include <unordered_set>
#include <cstdint>
#include "bgpinfo.pb.h"

namespace BGPInfo {

    /**
     * Represents AS relations: Up and Downstream Autonomous Systems for a given
     * Autonomous System. These are usually not part of a BGP dump but it was added
     * for convenience as it allows to fast list all neighbors of an Autonomous System.
     */
    class ASRelation {
    public:
        ASRelation();

        /**
         * Constructor: Takes the Autonomous System number to which the 
         * relations belong to. 
         * @param asn AS Number the relations belong to
         */
        ASRelation(uint32_t asn);

        /**
         * Constructor: Deserializes a asRelation.
         * @param asRelation AS relation in protobuf format
         */
        ASRelation(BGPInfo::PB::ASRelation asRelation);

        /**
         * The AS number the relations belong to
         * @return The AS number as integer
         */
        uint32_t getAsn() const;

        /**
         * The upstream AS neighbors
         * @return List of upstream ASes
         */
        std::vector<uint32_t> getLeft() const;


        /**
         * The downstream AS neighbors
         * @return List of downstream ASes
         */
        std::vector<uint32_t> getRight() const;

        /**
         * Add upstream ASN
         * @param left upstream ASN
         */
        void addLeft(uint32_t left);

        /**
         * Add downstream ASN
         * @param right downstream
         */
        void addRight(uint32_t right);
        /**
         * add up and downstream ASN
         * @param left upstream ASN
         * @param right downstream ASN
         */
        void addLeftRight(uint32_t left, uint32_t right);

        /**
         * Convert to protobuf message
         * @param asr
         */
        void toPB(BGPInfo::PB::ASRelation* asr) const;

        /**
         * Converts AS relation map to Protobuf ASRelationList
         * @param asRelations
         * @return 
         */
        static BGPInfo::PB::ASRelationList aSrelationsToPB(std::map<uint32_t, BGPInfo::ASRelation> asRelations);

        /**
         * Converts Protobuf ASRelationList to AS relation
         * @param pbPrefixList
         * @return 
         */
        static std::map<uint32_t, BGPInfo::ASRelation> pbToASrelations(BGPInfo::PB::ASRelationList pbASRelationList);

        /**
         * write AS relations map to file
         * @param asRelationList
         * @param filePath
         */
        static void saveASRelations(std::map<std::uint32_t, BGPInfo::ASRelation> asRelationList, std::string filePath);

        /**
         * loads AS relations map from file
         * @param filePath
         * @return 
         */
        static std::map<std::uint32_t, BGPInfo::ASRelation> loadASRelations(std::string filePath);

    private:
        uint32_t _asn; //AS Number
        std::vector<uint32_t> _left; //Upstream AS
        std::vector<uint32_t> _right; //Downstream AS
    };
}
#endif /* ASNRELATION_H */
