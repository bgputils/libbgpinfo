#ifndef BGPINFOUTIL_H
#define BGPINFOUTIL_H

#include <vector>
#include <sstream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cassert>
#include <iostream>
#include <cstdint>
#include <Poco/Net/IPAddress.h>
#include <bgpdump_attr.h>
#include <google/protobuf/message.h>

namespace BGPInfo {
    namespace Util {

        /**
         * Splits the string by the given delimeter
         * @param s The string to split
         * @param delim Delimeter
         * @return The split result as vector
         */
        std::vector<std::string> split(const std::string &s, char delim);

        /**
         * Splits the string by the given delimeter
         * @param s The string to split
         * @param delim Delimeter
         * @param elems The container to hold the split elements
         */
        void split(const std::string &s, char delim, std::vector<std::string> &elems);

        /**
         * Splits the string by the given delimeter. Increased performance compared to split
         * @param s The string to split
         * @param delim Delimeter
         * @return The split result as vector
         */
        std::vector<std::string> split2(std::string s, const char delimiter);

        /**
         * Parses the BGPDUMP_IP_ADDRESS as Poco::Net::IPAddress
         * @param ip The IP in BGPDUMP_IP_ADDRESS format
         * @param afi The IP version
         * @return The IP as Poco::Net::IPAddress
         */
        Poco::Net::IPAddress parseIP(BGPDUMP_IP_ADDRESS ip, int afi);

        /**
         * Parses the in_addr as Poco::Net::IPAddress
         * @param ip The IP in in_addr format
         * @return The IP as Poco::Net::IPAddress
         */
        Poco::Net::IPAddress parseIP(in_addr ip);

        /**
         * Parses the in6_addr as Poco::Net::IPAddress
         * @param ip The IP in in6_addr format
         * @return The IP as Poco::Net::IPAddress
         */
        Poco::Net::IPAddress parseIP(in6_addr ip);

        /**
         * Masks the IP with the given CIDR netmask
         * @param ip The IP to mask
         * @param mask The netmask in CIDR format
         * @return Masked IP address
         */
        Poco::Net::IPAddress maskIP(Poco::Net::IPAddress ip, unsigned mask);

        /**
         * Writes the Protobuf mesage to gz compressed binary file
         * @param message Protobuf message
         * @param filePath Target file
         */
        void writeProtoBufToFile(::google::protobuf::Message& message, std::string filePath);

        /**
         * Determines if path is a file
         * @param path The path to the file
         * @return Returns true if it is a file, false else
         */
        bool isFile(std::string path);

        /**
         * Determines if path is a directory
         * @param path The path to the directory
         * @return Returns true if it is a directory, false else
         */
        bool isDir(std::string path);

        /**
         * Determines if a given file exists
         * @param fileName The file in question
         * @return Indicates of the give file exists
         */
        bool fileExists(std::string fileName);

        std::string sanitizeDirectoryPath(std::string path);

        /**
         * Parses a protobuf repeated field into a std::vector of given type
         * @param field 
         * @return 
         */
        template<class T>
        std::vector<T> parseBPRepeatedFieldtoVector(::google::protobuf::RepeatedField<T> fields) {
            std::vector<T> fieldList;
            for (auto const& field : fields) {
                fieldList.emplace_back(field);
            }
            return fieldList;
        }

        /**
         * Parses a protobuf repeated field into a std::set of given type
         * @param field 
         * @return 
         */
        template<class T>
        std::set<T> parseBPRepeatedFieldtoSet(::google::protobuf::RepeatedField<T> fields) {
            std::set<T> fieldList;
            for (auto const& field : fields) {
                fieldList.insert(field);
            }
            return fieldList;
        }

    }
}
#endif /* BGPINFOUTIL_H */