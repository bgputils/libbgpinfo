#include <algorithm>
#include <iostream>    
#include <fstream>
#include <stdio.h>
#include <sys/stat.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>

#include "peer.h"
#include "util.h"
#include "filenotfoundexception.h"

BGPInfo::Peer::Peer() {
}

BGPInfo::Peer::Peer(Poco::Net::IPAddress ip, Poco::Net::IPAddress bgpId, uint32_t as) {
    _ip = ip;
    _bgpId = bgpId;
    _as = as;
}

BGPInfo::Peer::Peer(BGPInfo::PB::Peer peerPB)
: BGPInfo::Peer(Poco::Net::IPAddress::parse(peerPB.ip())
, Poco::Net::IPAddress::parse(peerPB.bgpid()), peerPB.as()) {
}

uint32_t BGPInfo::Peer::GetAs() {
    return _as;
}

Poco::Net::IPAddress BGPInfo::Peer::GetBgpId() {
    return _bgpId;
}

Poco::Net::IPAddress BGPInfo::Peer::GetIp() {
    return _ip;
}

void BGPInfo::Peer::toPB(BGPInfo::PB::Peer* p) const {
    p->set_ip(this->_ip.toString());
    p->set_bgpid(this->_bgpId.toString());
    p->set_as(this->_as);
}

BGPInfo::PB::PeerList BGPInfo::Peer::PeersToPB(std::vector<BGPInfo::Peer> peers) {

    BGPInfo::PB::PeerList pbPeerList;
    for (auto& peer : peers) {
        peer.toPB(pbPeerList.add_peers());
    }
    return pbPeerList;
}

std::vector<BGPInfo::Peer> BGPInfo::Peer::pbToPeers(BGPInfo::PB::PeerList pbPeerList) {
    std::vector<BGPInfo::Peer> peers;
    for (int i = 0; i < pbPeerList.peers_size(); i++) {
        const BGPInfo::PB::Peer& pbpeer = pbPeerList.peers(i);
        BGPInfo::Peer peer = BGPInfo::Peer(pbpeer);
        peers.push_back(peer);
    }
    return peers;
}

void BGPInfo::Peer::savePeers(std::vector<BGPInfo::Peer> peers, std::string filePath) {
    BGPInfo::PB::PeerList pbPeerList = PeersToPB(peers);
    Util::writeProtoBufToFile(pbPeerList, filePath);
}

std::vector<BGPInfo::Peer> BGPInfo::Peer::loadPeers(std::string filePath) {

    BGPInfo::PB::PeerList pbPeerList;
    int fd = open(filePath.c_str(), O_RDONLY);
    if (fd == -1) {
         throw BGPInfo::Util::FileNotFoundException(filePath);
    }
    google::protobuf::io::FileInputStream fis(fd);
    google::protobuf::io::GzipInputStream gis(&fis);
    google::protobuf::io::CodedInputStream cis(&gis);
    int buffSize = 1073741824;
    cis.SetTotalBytesLimit(buffSize, buffSize - 100000);
    {
        if (!pbPeerList.ParseFromCodedStream(&cis)) {
           throw std::runtime_error("Failed to parse prefixes.");
        }
    }

    return pbToPeers(pbPeerList);
}

bool BGPInfo::Peer::operator!=(const Peer& a) const {
    return this->_ip != a._ip;
}

bool BGPInfo::Peer::operator<(const Peer& a) const {
    return this->_ip < a._ip;
}

bool BGPInfo::Peer::operator<=(const Peer& a) const {
    return this->_ip <= a._ip;
}

bool BGPInfo::Peer::operator==(const Peer& a) const {
    return this->_ip == a._ip;
}

bool BGPInfo::Peer::operator>(const Peer& a) const {
    return this->_ip > a._ip;
}

bool BGPInfo::Peer::operator>=(const Peer& a) const {
    return this->_ip >= a._ip;
}
